function sendPayment() {
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  } else {
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
  }
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

    }
  }
  var expirationDate = $('input[name="expDateY"]').val() + $('input[name="expDateM"]').val();
  var params = {
    firstName: $('input[name="firstName"]').val(),
    lastName: $('input[name="lastName"]').val(),
    streetAddress: $('input[name="streetAddress"]').val(),
    streetAddress2: $('input[name="streetAddress2"]').val(),
    city: $('input[name="city"]').val(),
    state: $('input[name="state"]').val(),
    country: $('input[name="country"]').val(),
    zipCode: $('input[name="zipCode"]').val(),
    phone: $('input[name="phone"]').val(),
    email: $('input[name="email"]').val(),
    cardNumber: $('input[name="cardNumber"]').val(),
    expirationDate: expirationDate,
    ccv2: $('input[name="ccv2"]').val(),
    amountToSend: parseFloat($('input[name="amountToSend"]').val()),
    cardType: $('#cardType').val()
  }
  xmlhttp.open('POST','/api/payments',true);
  xmlhttp.setRequestHeader('Content-type','application/json');
  xmlhttp.send(JSON.stringify(params));
  $('#preloader').removeClass('hidden');
  xmlhttp.onload = function () {
    if (xmlhttp.status === 200) {
      document.open();
      document.write(xmlhttp.responseText);
    } else {
      $('#preloader').addClass('hidden');
      $('.alert').removeClass('hidden');
      $('#responseMsg').html(xmlhttp.responseText);
    }
  };
}

function validateEmail(email) {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

function isUsZipValid(zip) {
  return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zip);
}

function isUsPhoneValid(phone) {
  return /\d{3}[\-]\d{3}[\-]\d{4}/.test(phone);
}

function isValidDate() {
  var year = parseFloat($('.expDateY').val());
  var month = parseFloat($('.expDateM').val());
  var monthValid = false;
  var yearValid = false;
  var validCard = false;
  if ($('.expDateM').val().length == 2) {
    if (1 <= month && month <= 12) {
      monthValid = true;
    }
  }
  if (year >= new Date().getFullYear() % 100) {
    yearValid = true
  }
  if (monthValid && yearValid) {
    validCard = true
  }
  if (year == new Date().getFullYear() % 100) {
    if (month >= new Date().getMonth() + 1) {
      validCard = true;
    } else {
      validCard = false
    }
  }
  return validCard;
}

function isValidCardNumber (value, cb) {
  var validNumber = false;
  var card = $('#cardType').val();
  var self = $('input[name=cardNumber]').val().replace(/ /g, '').replace('_','');
  if (card == 'Visa' && self.charAt(0) == '4' && self.length == 16) {
    console.log("Visa");
    binCheck(self, cb);
  } else if (card == 'Master Card' && self.charAt(0) == '5' && self.length == 16) {
    binCheck(self, cb);
  } else {
    cb(false);
  }
}

function isValidAmount () {
  var self = $('input[name=amountToSend]').val();
  var max = parseFloat($('#singleTransactionLimit').text())
  var validAmount = false;
  if ($.isNumeric(self) && self <= max && self >= 0) {
    validAmount = true;
  }
  return validAmount
}

function isValidMoneyUpdate () {
  var self = $('.update-limit').val();
  var validAmount = false;
  if ($.isNumeric(self) && self >= 0) {
    validAmount = true;
  }
  return validAmount
}

function isValidName () {
  var self = $('input[name=firstName]').val();
  var validName = false;
  if (self.length) {
    validName = true;
  }
  return validName
}

function isValidLastName () {
  var self = $('input[name=lastName]').val();
  var valid = false;
  if (self.length) {
    valid = true;
  }
  return valid
}

function isValidStreet () {
  var self = $('input[name=streetAddress]').val();
  var valid = false;
  if (self.length) {
    valid = true;
  }
  return valid
}

function isValidCity () {
  var self = $('input[name=city]').val();
  var valid = false;
  if (self.length) {
    valid = true;
  }
  return valid
}

function isValidState () {
  var self = $('input[name=state]').val();
  var valid = false;
  if (self.length) {
    valid = true;
  }
  return valid
}

function isValidCCV () {
  var self = $('input[name=ccv2]').val();
  var valid = false;
  if (self.length) {
    valid = true;
  }
  return valid
}

function isValidMoneyNew () {
  var self = $('.new-user-limit').val();
  var validAmount = false;
  if ($.isNumeric(self) && self >= 0) {
    validAmount = true;
  }
  return validAmount
}

function tooltip($this, message, func, isAsync) {

  var tooltipResult = function (result) {
    if (result) {
        $this.parent('.form-group').removeClass('has-error');
        $this.parent('.form-group').addClass('has-success');
        $this.tooltip('destroy');
        $('#submitBtn').attr('disabled', false);

    } else {
        $this.parent('.form-group').removeClass('has-success');
        $this.parent('.form-group').addClass('has-error');
        $('#submitBtn').attr('disabled', true);
        if ($this.data && $this.data('bs.tooltip')) {
          if ($this.data('hidden.bs.tooltip')) {
            $this.tooltip('show');
          }
        } else {
          $this.tooltip({
            trigger: 'manual',
            title: message
          });
          $this.tooltip('show');
        }
      }
  }

  if (isAsync) {
    func($this.val, tooltipResult);
  } else {
    tooltipResult(func($this.val()));
  }
}

function tooltipDate($this, message, func) {
  if (func) {
      $this.parent('.form-group').removeClass('has-error');
      $this.parent('.form-group').addClass('has-success');
      $('#submitBtn').attr('disabled', false);

  } else {
      $this.parent('.form-group').removeClass('has-success');
      $this.parent('.form-group').addClass('has-error');
      $('#submitBtn').attr('disabled', true);
    }
}

function binCheck(cardNumber, cb) {
    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

      }
    }
    var params = {
      cardNumber: cardNumber
    }
    xmlhttp.open('POST','/api/payments/bin-check',true);
    xmlhttp.setRequestHeader('Content-type','application/json');
    xmlhttp.send(JSON.stringify(params));
    var valid = false
    xmlhttp.onload = function () {
      cb(xmlhttp.status === 200);
    };
}

function disableSendBtn(){
  $('#submitBtn').attr('disabled', true);
}

$(function() {
  $('input[name=email]').on('keyup change', function() {
    tooltip($(this), 'Invalid email.', validateEmail);
  });
  $('input[name=phone]').on('keyup change', function() {
    tooltip($(this), 'Invalid phone.', isUsPhoneValid);
  });
  $('input[name=zipCode]').on('keyup change', function() {
    tooltip($(this), 'Invalid zip code.', isUsZipValid);
  });
  $('input[name=expDateY]').on('keyup change', function() {
    tooltipDate($(this), 'Invalid expDateY.', isValidDate());
  });
  $('input[name=expDateM]').on('keyup change', function() {
    tooltipDate($(this), 'Invalid expDateM.', isValidDate());
  });
  $('input[name=cardNumber]').on('keyup change', function() {
    tooltip($(this), 'Invalid Card Number.', isValidCardNumber, true);
  });
  $('select[name=cardType]').on('keyup change', function() {
    tooltip($('input[name=cardNumber]'), 'Invalid Card Number.', isValidCardNumber, true);
  });
  $('input[name=amountToSend]').on('keyup change', function() {
  var max = parseFloat($('#singleTransactionLimit').text())
    tooltip($(this), 'max ' + max, isValidAmount);
  });
  $('.new-user-limit').on('keyup change', function() {
    tooltip($(this), '', isValidMoneyNew);
  });
  $('.update-limit').on('keyup change', function() {
    tooltip($(this), '', isValidMoneyUpdate);
  });
  $('input[name=firstName]').on('keyup change', function() {
    tooltip($(this), 'Name is required', isValidName);
  });
  $('input[name=lastName]').on('keyup change', function() {
    tooltip($(this), 'last name is required', isValidLastName);
  });
  $('input[name=streetAddress]').on('keyup change', function() {
    tooltip($(this), 'Street Address is required', isValidStreet);
  });
  $('input[name=city]').on('keyup change', function() {
    tooltip($(this), 'City is required', isValidCity);
  });
  $('input[name=state]').on('keyup change', function() {
    tooltip($(this), 'State is required', isValidState);
  });
  $('input[name=ccv2]').on('keyup change', function() {
    tooltip($(this), 'ccv2 is required', isValidCCV);
  });
});
$('.credit').mask("9999 9999 9999 9999");

$(document).ready(function() {
  $('#users tr')
  .filter(':has(:checkbox:checked)')
  .addClass('selected')
  .end()
  // .click(function(event) {
  //   if (event.target.type !== 'checkbox') {
  //     $(':checkbox', this).trigger('click');
  //   }
  // })
  .find(':checkbox')
  .click(function(event) {
    $(this).parents('tr:first').toggleClass('selected');
  });
  window.updateParam = {};
  $("tr").on( "click", ".btn", function() {
    var wrapper = $(this).closest('tr');
    window.updateParam.id = wrapper.find('.id').text();
    $('#user-update input[name="firstName"]').val(wrapper.find('.firstName').text());
    $('#user-update input[name="lastName"]').val(wrapper.find('.lastName').text());
    $('#user-update input[name="phoneNumber"]').val(wrapper.find('.phoneNumber').text());
    $('#user-update input[name="email"]').val(wrapper.find('.mail').text());
    $('#user-update select').val(wrapper.find('.userType').text());
    $('#user-update input[name="companyname"]').val(wrapper.find('.companyName').text());
    $('#user-update input[name="resourcename"]').val(wrapper.find('.resourceName').text());
    $('#user-update input[name="numberoftransactionsinitiated"]').val(wrapper.find('.initiatedTransactions').text());
    $('#user-update input[name="numberoftransactions"]').val(wrapper.find('.transaction').text());
    $('#user-update input[name="singletransactionlimit"]').val(wrapper.find('.singleTransaction').text());
  });

  $('#Create').click(function() {
    $('#new-user').modal('toggle');
    location.reload();
  });

});
function createUser() {
    if (window.XMLHttpRequest) {
      xmlhttp=new XMLHttpRequest();
    }
  else {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }
  }
  var createUserForm = document.getElementById("signupform");
  var params = {
    firstName: String = createUserForm["firstName"].value,
    lastName: String = createUserForm["lastName"].value,
    phoneNumber: String = createUserForm["phoneNumber"].value,
    email: String = createUserForm["email"].value,
    userType: String = createUserForm["userType"].value,
    companyName: String = createUserForm["companyname"].value,
    resourceName: String = createUserForm["resourcename"].value,
    numberOfTransactions: Number = parseInt(createUserForm["numberoftransactions"].value),
    singleTransactionLimit: Number = parseFloat(createUserForm["singletransactionlimit"].value),
    password: String = createUserForm["password"].value,
  }
  xmlhttp.open("POST","/api/admin",true);
  xmlhttp.setRequestHeader("Content-type","application/json");
  xmlhttp.send(JSON.stringify(params));
  console.log(JSON.stringify(params));
  setTimeout( function() {
    if (xmlhttp.status == 200)  {
      location.reload();
    } else {
      console.log("status = " + xmlhttp.status)
    }
  }, 2000)
};

function deleteSelectedUsers() {
  var ids = $("#users tbody tr.selected th").map(function() {
    return $(this).text();
  }).get()

  if (window.XMLHttpRequest) {
    xmlhttp=new XMLHttpRequest();
  }
  else {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {

    }
  }

  xmlhttp.open("DELETE","/api/admin",true);
  xmlhttp.setRequestHeader("Content-type","application/json");
  xmlhttp.send(JSON.stringify(ids));


  setTimeout( function() {
    if (xmlhttp.status == 200)  {
      location.reload();
    } else {
      console.log("status = " + xmlhttp.status)
    }
  }, 1000)

};

function updateUser() {
    if (window.XMLHttpRequest) {
      xmlhttp=new XMLHttpRequest();
    }
  else {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    }
  }
  var params = {
    originatorId: $('#user-update input[name="originatorId"]').val(),
    firstName: $('#user-update input[name="firstName"]').val(),
    lastName: $('#user-update input[name="lastName"]').val(),
    phoneNumber: $('#user-update input[name="phoneNumber"]').val(),
    email: $('#user-update input[name="email"]').val(),
    userType: $('#user-update select').val(),
    companyName: $('#user-update input[name="companyname"]').val(),
    resourceName: $('#user-update input[name="resourcename"]').val(),
    numberOfTransactions: Number = parseInt($('#user-update input[name="numberoftransactions"]').val()),
    singleTransactionLimit: Number = parseFloat($('#user-update input[name="singletransactionlimit"]').val()),
    password: $('#user-update input[name="password"]').val(),
  }
  xmlhttp.open("PUT","/api/admin/"+ window.updateParam.id ,true);
  xmlhttp.setRequestHeader("Content-type","application/json");
  xmlhttp.send(JSON.stringify(params));
  console.log(JSON.stringify(params));
  xmlhttp.onload = function () {
    if (xmlhttp.status === 200) {
      location.reload();
    }
  };
};

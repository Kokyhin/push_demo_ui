
import app.Starter
import app.global.ConfigLoader
import app.models.User
import play.api.{Application, GlobalSettings, Logger}

object Global extends GlobalSettings {
  override def onStart(app: Application): Unit = {
    Starter.onStart(app)
  }

  override def onStop(app: Application): Unit = {
    Starter.onStop(app)
  }

}
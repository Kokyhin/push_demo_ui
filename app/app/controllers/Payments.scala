package app.controllers

import app.models.JsonFormats._
import app.models.Payment
import app.models._
import play.api.libs.json._
import play.api.mvc._
import app.service.WildcardApi
import play.api.Logger

import scala.util.{Failure, Success, Try}

object Payments extends Controller with Secured {
  val logger = Logger(this.getClass)

  def sendPayment = withUser(parse.json) { user => implicit request =>
    request.body.validate[PaymentRequest].asOpt match {

      case Some(paymentRequest) =>
        Try {
          val payment = paymentRequest.toPayment(user.id)

          payment.sendPayment(user)

          Ok(app.views.html.transfered(user, payment.withHidedCardNumber))
        } match {
          case Success(status) => status
          case Failure(e) =>
            logger.error(s"Unexpected error: ${e.getMessage}")
            e.printStackTrace()
            InternalServerError(s"Unexpected error: ${e.getMessage}")
        }

      case None => BadRequest("Incorrect JSON")
    }
  }

  def getPayments = withUser(parse.json) { user => implicit request =>
    val payments = Payment.getPaymentsByUserId(user.id)
    Ok(Json.toJson(payments))

  }

  def binCheck = withUser(parse.json) { user => implicit request =>

    request.body.validate[CardNumberRequest].asOpt match {
      case Some(CardNumberRequest(cardNumber)) =>
        if (cardNumber.length == 16) Ok
        else UnprocessableEntity
      case None => BadRequest("Incorrect JSON")
    }
  }


}

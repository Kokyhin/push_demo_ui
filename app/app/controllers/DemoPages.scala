package app.controllers

import app.global.ConfigLoader
import app.models.{TransferValues, User}
import play.api.mvc.{Action, Controller}

object DemoPages extends Controller with Secured {

  val buildNumber = ConfigLoader.conf.getString("build.number")
  val originatorId = User.default.originatorId
  val environment = ConfigLoader.conf.getString("app.environment")

  def createPaymentPage = withUser { user => implicit request =>
    if (user.initiatedTransactions < user.numberOfTransactions)
      Ok(app.views.html.create(user.singleTransactionLimit, TransferValues.default))
    else
      Ok(app.views.html.error("The maximum number of transactions has been met. Please contact your Wildcard Payments contact."))
  }

  def indexPage = withUser { user => implicit request =>

    Ok(app.views.html.index(user))
  }

  def loginPage = Action {

    Ok(app.views.html.login())
  }

  def adminPanelPage = withUser { user => implicit request =>
    if (user.isAdmin)
      Ok(app.views.html.admin_panel(User.getAll, originatorId, environment, buildNumber))
    else
      Forbidden("Forbidden")
  }

}

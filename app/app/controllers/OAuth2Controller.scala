package app.controllers

import play.api.mvc.{Action, Controller}

object OAuth2Controller extends Controller{

  def oauth2Callback = Action {
    Ok("oauth2Callback")
  }

}

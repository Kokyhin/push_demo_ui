package app.controllers

import app.models.JsonFormats._
import app.models.User
import app.models.{UserRequest, UserResponse}
import org.bson.types.ObjectId
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}

object AdminPanel extends Controller with Secured {
  val AUTH_TOKEN: String = "authToken"

  def createUser = withUser(parse.json) { user => implicit request =>
    if (user.isAdmin) {
      request.body.validate[UserRequest].asOpt match {
        case Some(userRequest) =>
          User.createUser(userRequest) match {
            case Some(u) => Created(Json.toJson(UserResponse.fromUser(u)))
            case None => InternalServerError("Failed to create user")
          }
        case None =>
          BadRequest
      }
    } else Forbidden
  }

  def listUsers = Action { implicit request =>
    Ok(Json.toJson(User.getAll.map(u => UserResponse.fromUser(u))))
  }

  def updateUser(id: ObjectId) = withUser(parse.json) { user => implicit request =>
    if (user.isAdmin) {
      request.body.validate[UserRequest].asOpt match {
        case Some(userRequest) =>
          User.findOneById(id) match {
            case Some(u) =>
              val updatedUser =
                if (userRequest.password.nonEmpty) {
                  u.copy(password = User.getSha512(userRequest.password))
                } else {
                  u
                }.copy(
                  firstName = userRequest.firstName,
                  lastName = userRequest.lastName,
                  phoneNumber = userRequest.phoneNumber,
                  userType = userRequest.userType,
                  companyName = userRequest.companyName,
                  resourceName = userRequest.resourceName,
                  numberOfTransactions = userRequest.numberOfTransactions,
                  singleTransactionLimit = userRequest.singleTransactionLimit,
                  email = userRequest.email
                )
              User.save(updatedUser)
              Ok(Json.toJson(UserResponse.fromUser(updatedUser)))
            case None => NotFound
          }
        case None => BadRequest
      }
    } else Forbidden
  }

  def deleteUsers() = withUser(parse.json) { user => implicit request =>
    if (user.isAdmin)
      request.body.asOpt[List[ObjectId]] match {
        case Some(ids) => ids.map(id => User.removeById(id))
          Ok
        case None => BadRequest
      }
    else Forbidden
  }

}

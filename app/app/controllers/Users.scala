package app.controllers

import app.models.JsonFormats._
import app.models.{User, _}
import play.api.mvc._


object Users extends Controller with Secured {
  val AUTH_TOKEN: String = "authToken"

  def signIn = Action(parse.json) { implicit request =>
    request.body.validate[LoginRequest].asOpt match {
      case Some(loginRequest) =>
        User.authenticate(loginRequest.email, loginRequest.password) match {
          case Some(user) => val token = User.selectAuthToken(user)
            Ok.withSession(AUTH_TOKEN -> token)
          case None => Unauthorized
        }
      case None => BadRequest
    }
  }

  def signOut = withUser { user => implicit request =>
    val token = request.session(AUTH_TOKEN)
    User.signOut(user, token)
    Ok
  }
}




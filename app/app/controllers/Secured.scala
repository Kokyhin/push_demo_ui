package app.controllers

import app.models.User
import org.bson.types.ObjectId
import play.api.mvc._

import scala.concurrent.Future

trait Secured {

  def token(request: RequestHeader) = request.session.get(Users.AUTH_TOKEN)

  def onUnauthorized(request: RequestHeader) = Results.Redirect("/login")
  def asyncOnUnauthorized(request: RequestHeader) = Future.successful(Results.Redirect("/demo/login.html"))

  def withAuth[A](bp:BodyParser[A])(f: => String => Request[A] => Result) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action(bp)(request => f(token)(request))
    }
  }

  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action(request => f(token)(request))
    }
  }

  def asyncWithAuth[A](bp:BodyParser[A])(f: => String => Request[A] => Future[Result]) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action.async(bp)(request => f(token)(request))
    }
  }

  def asyncWithAuth(f: => String => Request[AnyContent] => Future[Result]) = {
    Security.Authenticated(token, onUnauthorized) { token =>
      Action.async(request => f(token)(request))
    }
  }

  def asyncWithUser(f: User => Request[AnyContent] => Future[Result]) = asyncWithAuth { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(asyncOnUnauthorized(request))
  }

  def asyncWithUser[A](bp: BodyParser[A])(f: User => Request[A] => Future[Result]) = asyncWithAuth(bp) { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(asyncOnUnauthorized(request))
  }

  def withUser(f: User => Request[AnyContent] => Result) = withAuth { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(onUnauthorized(request))
  }

  def withUser[A](bp: BodyParser[A])(f: User => Request[A] => Result) = withAuth(bp) { token => implicit request =>
    User.findOneByAuthToken(token).map { user =>
      f(user)(request)
    }.getOrElse(onUnauthorized(request))
  }

  def isValidObjectId(id: String)(fOk: => Result) = {
    if (ObjectId.isValid(id)) fOk else Results.BadRequest("Incorrect ObjectId")
  }
}

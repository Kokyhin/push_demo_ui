package app.models

import app.global.ConfigLoader
import app.service.{Customer, Transfer, WildcardApi}
import app.utils.Mailer
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.WriteConcern
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import org.bson.types.ObjectId

case class Payment(
                    id: ObjectId = new ObjectId(),
                    userId: ObjectId,
                    firstName: String,
                    lastName: String,
                    streetAddress: String,
                    streetAddress2: String,
                    city: String,
                    state: String,
                    country: String,
                    zipCode: String,
                    phone: String,
                    email: String,
                    accountType: String,
                    cardNumber: String,
                    expirationDate: String,
                    ccv2: String,
                    amountToSend: Double
                  ) {
  def sendPayment(user: User) = {
    val originatorId = User.default.originatorId

    val originatorFIs = WildcardApi.getFundingInstruments(originatorId)
    val customer = WildcardApi.createCustomer(
      originatorId,
      Customer.fromPayment(this)
    )
    val destFundingInstrument = WildcardApi.createCustomerFundingInstrument(originatorId, customer.id.get, this)
    val transfer =
      Transfer.create(
        originatorId = originatorId,
        srcFundingInstrumentId = originatorFIs.head.id.get,
        customerId = customer.id.get,
        destFundingInstrumentId = destFundingInstrument.id.get,
        amount = amountToSend.toDouble
      )
    WildcardApi.sendTransfer(originatorId, transfer)

    Payment.save(this)
    User.save(user.copy(initiatedTransactions = user.initiatedTransactions + 1))


    val securedPayment = this.copy(cardNumber = cardNumber.substring(cardNumber.length - 4))
    Mailer.sendMail(
      to = s"$firstName $lastName",
      email = email,
      subject = ConfigLoader.conf.getString("mail.customer.subject"),
      text = app.views.html.email_to_customer(user, securedPayment).toString()
    )
    Mailer.sendMail(
      to = s"${user.firstName} ${user.lastName}",
      email = user.email,
      subject = ConfigLoader.conf.getString("mail.originator.subject"),
      text = app.views.html.email_to_originator(user, securedPayment).toString()
    )

  }

  def withHidedCardNumber = {
    this.copy(cardNumber = Payment.hideCardNumber(cardNumber))
  }
}

object Payment extends PaymentDAO {
  def hideCardNumber(cardNumber: String) = {
    val s = cardNumber
    s.substring(0, s.length - 4).replaceAll("[0-9]", "X") + s.substring(s.length - 4, s.length)
  }
}

trait PaymentDAO extends ModelCompanion[Payment, ObjectId] {

  def collection = Mongo()("payments")

  override def defaultWriteConcern: WriteConcern = WriteConcern.Safe

  val dao = new SalatDAO[Payment, ObjectId](collection) {}

  def getPaymentsByUserId(userId: ObjectId) = find(MongoDBObject("userId" -> userId)).toList

}

case class TransferValues(
                             firstName :String,
                             lastName :String,
                             streetAddress :String,
                             streetAddress2 :String,
                             city :String,
                             country :String,
                             state :String,
                             zipCode :String,
                             email :String,
                             phone :String,
                             cardNumber :String,
                             expDateM :String,
                             expDateY :String,
                             ccv2 :String,
                             amountToSend :String
                         )
object TransferValues {
  private val conf = ConfigLoader.conf.getConfig("transfer.values")
  val default = TransferValues(
    firstName = conf.getString("firstName"),
    lastName = conf.getString("lastName"),
    streetAddress = conf.getString("streetAddress"),
    streetAddress2 = conf.getString("streetAddress2"),
    city = conf.getString("city"),
    country = conf.getString("country"),
    state = conf.getString("state"),
    zipCode = conf.getString("zipCode"),
    email = conf.getString("email"),
    phone = conf.getString("phone"),
    cardNumber = conf.getString("cardNumber"),
    expDateM = conf.getString("expDateM"),
    expDateY = conf.getString("expDateY"),
    ccv2 = conf.getString("ccv2"),
    amountToSend = conf.getString("amountToSend")
  )
}
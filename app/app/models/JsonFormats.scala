package app.models

import app.service.AuthErrorResponse
import org.bson.types.ObjectId
import play.api.libs.json._

object JsonFormats {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {

    def reads(json: JsValue) = {
      json match {
        case jsString: JsString =>
          if (ObjectId.isValid(jsString.value)) JsSuccess(new ObjectId(jsString.value))
          else JsError("Invalid ObjectId")
        case other => JsError("Can't parse json path as an ObjectId. Json content = " + other.toString())
      }
    }

    def writes(oId: ObjectId): JsValue = JsString(oId.toString)
  }

  implicit val userResponseFormat = Json.format[UserResponse]
  implicit val userRequestFormat = Json.format[UserRequest]
  implicit val paymentFormat = Json.format[Payment]
  implicit val validationResultFormat = Json.format[ValidationResult]
  implicit val loginRequestFormat = Json.format[LoginRequest]
  implicit val errorResponseFormat = Json.format[ErrorResponse]
  implicit val authErrorResponseFormat = Json.format[AuthErrorResponse]
  implicit val paymentRequestFormat = Json.format[PaymentRequest]
  implicit val cardNumberRequestFormat = Json.format[CardNumberRequest]
}

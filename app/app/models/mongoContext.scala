package app

import com.novus.salat.{TypeHintFrequency, StringTypeHintStrategy, Context}

package object models {
  implicit val context = {
    val context = new Context {
      val name = "app/controllers/global"
      override val typeHintStrategy = StringTypeHintStrategy(when = TypeHintFrequency.WhenNecessary, typeHint = "_t")
    }
    context.registerGlobalKeyOverride(remapThis = "id", toThisInstead = "_id")
    context.registerClassLoader(this.getClass.getClassLoader)
    context
  }
}
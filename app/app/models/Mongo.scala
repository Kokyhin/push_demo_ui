package app.models

import com.mongodb.casbah.{MongoClient, MongoDB}
import app.global.ConfigLoader.conf

object Mongo {
  def apply(): MongoDB = {
    val mongoConf = conf.getConfig("mongodb.default")
    val dbName = mongoConf.getString("db")
    val host = mongoConf.getString("host")
    val port = mongoConf.getInt("port")

    MongoClient(host, port)(dbName)
  }
}

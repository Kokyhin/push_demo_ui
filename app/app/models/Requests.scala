package app.models

import java.util.Date

import org.bson.types.ObjectId
import play.i18n.Messages

import scala.collection.mutable

/*{"email":"admin","companyName":"admin","resourceName":"admin","numberOfTransactions":100,"singleTransactionLimit":100,"password":"admin"}
*/
case class UserRequest(
                        firstName: String,
                        lastName: String,
                        phoneNumber: String,
                        companyName: String,
                        userType: String,
                        resourceName: String,
                        numberOfTransactions: Int,
                        singleTransactionLimit: Double,
                        email: String,
                        password: String
                      ) {

  private val EmailRegex = "^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"
  private val PasswordRegex = "^.{6,18}$"

  def validate(): List[ValidationResult] = {
    val result = mutable.MutableList[ValidationResult]()

    if (!email.matches(EmailRegex)) result += ValidationResult("email", Messages.get("user.email"))
    if (User.findOneByEmail(email).nonEmpty) result += ValidationResult("email", Messages.get("user.email.unique"))
    if (!password.matches(PasswordRegex)) result += ValidationResult("password", Messages.get("user.password"))
    result.toList
  }

  def createUser: User = {
    User(
      firstName = firstName,
      lastName = lastName,
      phoneNumber = phoneNumber,
      userType = userType,
      companyName = companyName,
      resourceName = resourceName,
      initiatedTransactions = 0,
      numberOfTransactions = numberOfTransactions,
      singleTransactionLimit = singleTransactionLimit,
      email = email,
      password = User.getSha512(password)

    )
  }
}

case class PaymentRequest(
                           firstName: String,
                           lastName: String,
                           streetAddress: String,
                           streetAddress2: String,
                           city: String,
                           state: String,
                           country: String,
                           zipCode: String,
                           phone: String,
                           email: String,
                           cardType: String,
                           cardNumber: String,
                           expirationDate: String,
                           ccv2: String,
                           amountToSend: Double
                         ) {
  def toPayment(userId: ObjectId): Payment =
    Payment(
      userId = userId,
      firstName = firstName,
      lastName = lastName,
      streetAddress = streetAddress,
      streetAddress2 = streetAddress2,
      city = city,
      state = state,
      country = country,
      zipCode = zipCode,
      phone = phone,
      email = email,
      accountType = cardType,
      cardNumber = cardNumber,
      expirationDate = expirationDate,
      ccv2 = ccv2,
      amountToSend = amountToSend
    )
}

case class ValidationResult(field: String, message: String)

case class LoginRequest(email: String, password: String)

case class UserResponse(
                         email: String,
                         companyName: String,
                         resourceName: String,
                         numberOfTransactions: Int,
                         singleTransactionLimit: Double,
                         creationDate: Date
                       )

object UserResponse {
  def fromUser(user: User): UserResponse = {
    UserResponse(
      email = user.email,
      companyName = user.companyName,
      resourceName = user.resourceName,
      numberOfTransactions = user.numberOfTransactions,
      singleTransactionLimit = user.singleTransactionLimit,
      creationDate = user.creationDate
    )
  }
}


case class ErrorResponse(code: Int, message: String)


case class CardNumberRequest(cardNumber:String)
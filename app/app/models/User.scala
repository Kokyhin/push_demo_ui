package app.models

import java.io.UnsupportedEncodingException
import java.security.{MessageDigest, NoSuchAlgorithmException}
import java.util.{Date, UUID}

import com.mongodb.casbah.Imports._
import com.mongodb.casbah.WriteConcern
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import app.global.ConfigLoader.conf
import play.api.Logger
import play.api.libs.Crypto

case class User(
                 id: ObjectId = new ObjectId,
                 firstName:String,
                 lastName:String,
                 phoneNumber:String,
                 userType: String,
                 companyName: String,
                 resourceName: String,
                 initiatedTransactions: Int,
                 numberOfTransactions: Int,
                 singleTransactionLimit: Double,
                 email: String = "",
                 creationDate: Date = new Date,
                 password: Array[Byte] = null,
                 authTokens: Map[String, Long] = Map[String, Long]()
               ) {
  def isAdmin = {
    userType == "admin"
  }
}

object User extends UserDAO with UserUtil {


  import scala.concurrent.duration._

  lazy val userConf = conf.getConfig("user")
  private lazy val tokenLifeTimeDays = userConf.getInt("token_lifetime_days")
  val TokenLifeTimeMillis = tokenLifeTimeDays.days.toMillis

  object default {
    val defaultUserConf = userConf.getConfig("default_user")
    val email = defaultUserConf.getString("email")
    val originatorId = defaultUserConf.getString("originatorId")
    val firstName = defaultUserConf.getString("firstName")
    val lastName = defaultUserConf.getString("lastName")
    val phoneNumber = defaultUserConf.getString("phoneNumber")
    val userType = defaultUserConf.getString("userType")
    val companyName = defaultUserConf.getString("companyName")
    val resourceName = defaultUserConf.getString("resourceName")
    val numberOfTransactions = defaultUserConf.getInt("numberOfTransactions")
    val singleTransactionLimit = defaultUserConf.getDouble("singleTransactionLimit")
    val password = Crypto.decryptAES(defaultUserConf.getString("password"))
  }

}

trait UserDAO extends ModelCompanion[User, ObjectId] {

  val logger = Logger(this.getClass)

  def collection = Mongo()("users")

  val dao = new SalatDAO[User, ObjectId](collection) {}

  // Indexes
  collection.createIndex(DBObject("email" -> 1), DBObject("unique" -> true))

  override def defaultWriteConcern: WriteConcern = WriteConcern.Safe

  // Queries
  def authenticate(username: String, password: String): Option[User] = findOne(
    DBObject("email" -> username, "password" -> User.getSha512(password))
  )

  def findOneByEmail(email: String): Option[User] = findOne(MongoDBObject("email" -> email))

  // Unused
  def findOneByAuthToken(authToken: String): Option[User] = findOne(
    DBObject(s"authTokens.$authToken" -> DBObject("$exists" -> true))
  )

  def getAll = {
    findAll().toList
  }

  def createAdminUserInDB() = {

    save(createAdminUser, WriteConcern.Safe)
    logger.info("admin user created")
  }

  def createAdminUser = {
    User(
      firstName = User.default.firstName,
      lastName = User.default.lastName,
      phoneNumber = User.default.phoneNumber,
      userType = User.default.userType,
      companyName = User.default.companyName,
      resourceName = User.default.resourceName,
      initiatedTransactions = 0,
      numberOfTransactions = User.default.numberOfTransactions,
      singleTransactionLimit = User.default.singleTransactionLimit,
      email = User.default.email,
      password = User.getSha512(User.default.password)
    )
  }

}


trait UserUtil {

  // Our auth token
  def createAuthToken(user: User): String = {
    val token = UUID.randomUUID.toString
    val authenticatedUser = user.copy(authTokens = user.authTokens + (token -> System.currentTimeMillis()))
    User.save(authenticatedUser, WriteConcern.Safe)
    token
  }

  def selectAuthToken(user: User): String = {
    if (user.authTokens.nonEmpty && (user.authTokens.last._2 + User.TokenLifeTimeMillis > System.currentTimeMillis() - 50000)) {
      user.authTokens.last._1
    } else createAuthToken(user)
  }

  def deleteAuthToken(user: User, token: String): User = user.copy(authTokens = user.authTokens.filterNot(_._1 == token))

  def signOut(user: User, token: String) = User.save(User.deleteAuthToken(user, token), WriteConcern.Safe)

  // User
  def createUser(userRequest: UserRequest): Option[User] = {
    val user = userRequest.createUser
    User.save(user, WriteConcern.Safe)
    User.findOneById(user.id)
  }

  def deleteUser(user: User) {
    User.removeById(user.id)
  }

  //Password encryption
  def getSha512(value: String): Array[Byte] = try {
    MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"))
  } catch {
    case e: NoSuchAlgorithmException => throw new RuntimeException(e)
    case e: UnsupportedEncodingException => throw new RuntimeException(e)
  }
}
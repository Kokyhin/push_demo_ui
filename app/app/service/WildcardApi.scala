package app.service

import app.global.ConfigLoader.conf
import app.models.Payment
import play.api.Logger
import play.api.Play.current
import play.api.libs.json._
import play.api.libs.ws._
import app.service.Implicits._
import app.service.InternalApiJsonFormats._
import app.service.WildcardApi.ErrorResponse
import play.api.libs.Crypto

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}


object InternalApiJsonFormats {
  implicit val FeesFormat = Json.format[Fees]
  implicit val SponsorBankFormat = Json.format[SponsorBank]
  implicit val FundingInstrumentFormat = Json.format[FundingInstrument]
  implicit val MailingAddressFormat = Json.format[MailingAddress]
  implicit val OriginatorFormat = Json.format[Originator]
  implicit val CustomerFormat = Json.format[Customer]
  implicit val FundsFormat = Json.format[Funds]
  implicit val FundingDestinationFormat = Json.format[FundingDestination]
  implicit val FundingSourceFormat = Json.format[FundingSource]
  implicit val TransferFormat = Json.format[Transfer]
  implicit val errorResponseFormat = Json.format[ErrorResponse]
}

object Implicits {

  implicit def futureResponseToResponse(future: Future[WSResponse]): WSResponse = Await.result(future, 2.minutes)
}

object WildcardApi {

  val logger = Logger(this.getClass)

  val apiConf = conf.getConfig("wildcard.api")
  val aggregatorId = apiConf.getString("aggregator.id")
  val baseUrl = s"${apiConf.getString("baseUrl")}${apiConf.getString("aggregator.urlPostfix")}"


  case class ErrorResponse(
                            serviceCode: Int,
                            message: String,
                            href: String,
                            apiVersion: String,
                            appVersion: String
                          )

  def getRequest(endpoint: String) = {
    WS.url(s"$baseUrl/$aggregatorId/$endpoint").withQueryString("access_token" -> OAuth2.getTokenContainer.access_token)
  }

  def getOriginatorById(id: String): Originator = {
    getRequest(s"originators/$id").get()
      .json.\("originators")
      .as[List[Originator]].head
  }

  def getOriginators: List[Originator] = {
    getRequest(s"originators").get()
      .json.\("originators")
      .as[List[Originator]]
  }

  def getFundingInstruments(originatorId: String) = {
    val json = getRequest(s"originators/$originatorId/funding-instruments").get().json
//    logger.info(s"found originator funding instruments: \n $json")
    json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => json.\("funding_instruments").as[List[FundingInstrument]]
    }

  }

  def getCustomers(originatorId: String) = {
    val request = getRequest(s"originators/$originatorId/customers")
    val json = request.get().json
    json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => json.\("customers").as[List[Customer]]
    }
  }

  def createCustomer(originatorId: String, customer: Customer) = {
    val jsonCustomer = Json.toJson(customer.cleanPhoneNumbers)
    logger.info(jsonCustomer.toString())
    val response: WSResponse =
      getRequest(s"originators/$originatorId/customers").post(jsonCustomer)
    Try {response.json.asOpt[ErrorResponse]}.toOption.flatten match {
      case Some(error) => throw new RuntimeException(error.message)
      case None =>
        response.header("location") match {
          case Some(location) =>
            val customerId = location.substring(location.lastIndexOf("/") + 1)
            logger.info("customer id = " + customerId)
            getCustomerById(originatorId, customerId)
          case None =>
            throw new RuntimeException(
              s"Can't create Customer: \n " +
                s"request url: ${getRequest(s"originators/$originatorId/customers").url} \n" +
                s"request body: ${jsonCustomer.toString()} \n" +
                s"status: ${response.status}\n " +
                s"response body: ${response.body} \n "
            )
        }
    }
  }

  def getCustomerById(originatorId: String, customerId: String) = {
    val json = getRequest(s"originators/$originatorId/customers/$customerId").get().json
    json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => json.\("customers").as[List[Customer]].head
    }
  }


  def getCustomerFundingInstruments(originatorId: String, customerId: String): List[FundingInstrument] = {
    val json = getRequest(s"originators/$originatorId/customers/$customerId/funding-instruments").get().json
    json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => json.\("funding_instruments").as[List[FundingInstrument]]
    }


  }

  def getCustomerFundingInstrumentById(originatorId: String, customerId: String, fundingInstrumentId: String) = {
    val json = getRequest(s"originators/$originatorId/customers/$customerId/funding-instruments/$fundingInstrumentId").get().json
    json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => json.\("funding_instruments").as[List[FundingInstrument]].head
    }
  }

  def createCustomerFundingInstrument(originatorId: String, customerId: String, p: Payment): FundingInstrument = {
    val fundingInstrument = FundingInstrument(
      id = None,
      `type` = Some("debitcard"),
      default = Some(true),
      card_number = Some(p.cardNumber.replaceAllLiterally(" ", "")),
      expDate = Some(p.expirationDate),
      card_type = None,
      routing_number = None,
      account_number = None,
      account_type = None
    )
    logger.info("createCustomerFundingInstrument")
    logger.info("originatorId: " + originatorId)
    logger.info("customerId: " + customerId)
    logger.info(s"fundingInstrument: ${Json.prettyPrint(Json.toJson(fundingInstrument))}")
    val response: WSResponse =
      getRequest(s"originators/$originatorId/customers/$customerId/funding-instruments")
        .post(Json.toJson(fundingInstrument))

    Try{response.json.asOpt[ErrorResponse]}.toOption.flatten match {
      case Some(error) => throw new RuntimeException(error.message)
      case None =>
        response.header("location") match {
          case Some(location) =>
            val fundingInstrumentId = location.substring(location.lastIndexOf("/") + 1)
            logger.info("fundingInstrumentId = " + fundingInstrumentId)
            getCustomerFundingInstrumentById(originatorId, customerId, fundingInstrumentId)
          case None => throw new RuntimeException(
            s"""error in funding instrument creating:
               | response status = ${response.status}
               | response body = ${response.body}""".mkString
          )

        }

    }
  }

  def sendTransfer(originatorId: String, transfer: Transfer) = {
    logger.info("sendTransfer request body: ")
    logger.info(Json.prettyPrint(Json.toJson(transfer)))
    val req = getRequest(s"originators/$originatorId/transfers")
    logger.info("sendTransfer request url: ")
    logger.info(req.url)
    val response: WSResponse = req.post(Json.toJson(transfer))
    Try {
    response.json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None =>
        logger.info("sendTransfer result:")
        logger.info(response.body)
    }
    } match {
      case Success(_) =>
      case Failure(t) => throw new RuntimeException(response.body,t)
    }
  }

  var customersIds: Map[String, String] = Map.empty[String, String]

  def binCheck(originatorId: String, cardNumber: String): Boolean = {
    val customerId = customersIds.get(originatorId) match {
      case Some(id) => id
      case None => getCustomers(originatorId).headOption match {
        case Some(c) =>
          customersIds += (originatorId -> c.id.get)
          c.id.get
        case None => throw new RuntimeException()
      }
    }
    val cn = cardNumber.replaceAllLiterally(" ", "")
    val response: WSResponse =
      getRequest(s"originators/$originatorId/customers/$customerId/funding-instruments/bin/$cn")
        .get()
    logger.info(response.toString)
    logger.info(response.body)
    response.json.asOpt[ErrorResponse] match {
      case Some(error) => throw new RuntimeException(error.message)
      case None => response.status == 200 && response.json.\("bin").toOption.nonEmpty
    }

  }

}

case class Transfer(
                     id: Option[String],
                     state: Option[String],
                     txn_method: Option[String],
                     statement_description: String,
                     originator_reference: String,
                     intent: String,
                     effective_date: String,
                     funding_source: FundingSource,
                     funding_destination: FundingDestination,
                     funds: Funds,
                     response_code: Option[String],
                     response_reason: Option[String],
                     response_dsc: Option[String]

                   )

object Transfer {
  def create(originatorId: String, customerId: String, srcFundingInstrumentId: String, destFundingInstrumentId: String, amount: Double) = {
    Transfer(
      id = None,
      state = None,
      txn_method = Some("rtp"),
      statement_description = "testing",
      originator_reference = "receivingDfiId-0002RTP0",
      intent = "debit",
      effective_date = "2016-05-24T00:00:00",
      funding_source = FundingSource(originatorId, Some(srcFundingInstrumentId)),
      funding_destination = FundingDestination(customerId, Some(destFundingInstrumentId)),
      funds = Funds(amount, "USD"),
      response_code = None,
      response_reason = None,
      response_dsc = None
    )
  }
}

case class FundingSource(
                          originator_id: String,
                          funding_instrument_id: Option[String] = None
                        )

case class FundingDestination(
                               customer_id: String,
                               funding_instrument_id: Option[String]
                             )

case class Funds(
                  amount: Double,
                  currency_code: String
                )

case class Originator(
                       id: String,
                       company_name: String,
                       tin: String,
                       merchant_id: Option[String],
                       MMC: Option[String],
                       status: String,
                       default_currency: String,
                       default_time_zone: String,
                       created: Option[String],
                       mailing_address: MailingAddress,
                       funding_instrument: FundingInstrument,
                       sponsor_bank: SponsorBank,
                       payment_channels: String,
                       processing_cut_off_time: String,
                       fees: Fees,
                       `OFAC-check-passed`: Boolean,
                       velocity_daily: Int,
                       velocity_weekly: Int

                     )


case class MailingAddress(
                           line_1: Option[String],
                           line_2: Option[String],
                           locality: Option[String],
                           admin_district_level_1: Option[String],
                           country_code: Option[String],
                           postal_code: Option[String]
                         )

case class FundingInstrument(
                              id: Option[String],
                              default: Option[Boolean],
                              card_number: Option[String],
                              expDate: Option[String],
                              card_type: Option[String],
                              account_number: Option[String],
                              routing_number: Option[String],
                              account_type: Option[String],
                              `type`: Option[String]
                            )

case class SponsorBank(
                        name: String,
                        ODFI_GID: Option[String],
                        RDFI_GID: Option[String],
                        FDIC_certificate: String
                      )

case class Fees(
                 ACH_debit: Double,
                 ACH_credit: Double,
                 ACH_returns: Double,
                 ACH_notify_of_charge: Double,
                 ACH_notify_of_charge_violate: Double,
                 unauthorized_ret_verify: Double
               )

case class Customer(
                     id: Option[String],
                     `type`: Option[String],
                     first_name: String,
                     last_name: String,
                     email: Option[String],
                     phone_number_home: String,
                     phone_number_work: Option[String],
                     phone_number_cell: Option[String],
                     dob: String,
                     ssn: String,
                     mailing_address: MailingAddress,
                     OFAC_CheckDate: Option[String],
                     OFAC_ProbableMatch: Option[Boolean],
                     KYC_status: Option[String],
                     status: Option[String],
                     created: Option[String],
                     funding_instrument: Option[FundingInstrument]
                   ) {
  def cleanPhoneNumbers: Customer = {
    copy(
      phone_number_home = phone_number_home.replaceAllLiterally("-", ""),
      phone_number_work = phone_number_work.map(_.replaceAllLiterally("-", "")),
      phone_number_cell = phone_number_cell.map(_.replaceAllLiterally("-", ""))
    )
  }
}

object Customer {
  def fromPayment(p: Payment) = {
    val mailingAddress = MailingAddress(
      line_1 = Some(p.streetAddress),
      line_2 = Some(p.streetAddress2),
      locality = Some(p.city),
      admin_district_level_1 = Some(p.state),
      country_code = Some(p.country),
      postal_code = Some(p.zipCode)
    )
    Customer(
      id = None,
      `type` = Some("individual"),
      first_name = p.firstName,
      last_name = p.lastName,
      email = Some(p.email),
      phone_number_home = p.phone,
      phone_number_work = Some(p.phone),
      phone_number_cell = Some(p.phone),
      dob = "12-23-1999",
      ssn = "000-00-0000",
      mailing_address = mailingAddress,
      OFAC_CheckDate = None,
      OFAC_ProbableMatch = None,
      KYC_status = None,
      status = None,
      created = None,
      funding_instrument = None
    )
  }
}


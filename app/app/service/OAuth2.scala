package app.service

import org.openqa.selenium.htmlunit.HtmlUnitDriver
import play.api.Logger
import play.api.libs.concurrent.Akka
import play.api.libs.json.Json

import scala.concurrent.duration._
import play.api.Play.current

import scala.concurrent.ExecutionContext.Implicits.global
import app.global.ConfigLoader
import play.api.libs.Crypto
import app.models.JsonFormats._

import scala.util.{Failure, Success, Try}


object OAuth2 {

  implicit val tokenContainerFormat = Json.format[TokenContainer]

  val wildcardApiConf = ConfigLoader.conf.getConfig("wildcard.api")
  val oAuthbaseUrl =
    s"${wildcardApiConf.getString("baseUrl")}${wildcardApiConf.getString("oAuth2.urlPostfix")}"
  val authorizeUrl = s"$oAuthbaseUrl/authorize"
  val response_type = "code"
  val client_id = wildcardApiConf.getString("oAuth2.client_id")
  val redirect_uri = wildcardApiConf.getString("oAuth2.redirect_uri")

  val username = wildcardApiConf.getString("oAuth2.username")
  val password = Crypto.decryptAES(wildcardApiConf.getString("oAuth2.password"))

  val accessTokenUrl = s"$oAuthbaseUrl/access_token"
  val client_secret = Crypto.decryptAES(wildcardApiConf.getString("oAuth2.client_secret"))

  val logger = Logger(this.getClass)

  val driver = new HtmlUnitDriver()

  private var tokenContainer: Option[TokenContainer] = None

  def authorize(): TokenContainer = {
    driver.get(s"$authorizeUrl?response_type=$response_type&client_id=$client_id&redirect_uri=$redirect_uri")
    logger.info(s"authorize: driver.getCurrentUrl = ${driver.getCurrentUrl}")
    val usernameForm = driver.findElementById("username")
    usernameForm.click()
    usernameForm.sendKeys(username)
    val passwordForm = driver.findElementById("password")
    passwordForm.click()
    passwordForm.sendKeys(password)
    driver.findElementById("login-button").click()

    val urlWithCode = driver.getCurrentUrl
    logger.info(s"authorize: urlWithCode = $urlWithCode")

    val code = urlWithCode.substring(urlWithCode.lastIndexOf("code=") + 5)
    logger.info(s"authorize: code = $code")

    driver.get(s"$accessTokenUrl?client_id=$client_id&client_secret=$client_secret&grant_type=authorization_code&code=$code&redirect_uri=$redirect_uri")
    logger.info(s"authorize: driver.getCurrentUrl = ${driver.getCurrentUrl}")
    logger.info(s"authorize: token response ${driver.getPageSource}")
    val json = driver.getPageSource
    val container = Json.parse(driver.getPageSource).asOpt[AuthErrorResponse] match {
      case Some(error) => throw new RuntimeException(s"OAuth2 error: ${error.error} ${error.error_description}")
      case None => Json.parse(driver.getPageSource).as[TokenContainer]
    }

    Akka.system.scheduler.scheduleOnce((container.expires_in - 300).seconds) {
      refreshToken()
    }
    container

  }

  def refreshToken(): Unit = {
    driver.get(s"$accessTokenUrl?client_id=$client_id&client_secret=$client_secret&grant_type=refresh_token&refresh_token=${getTokenContainer.refresh_token}&redirect_uri=$redirect_uri")
    tokenContainer = Some(Json.parse(driver.getPageSource).as[TokenContainer])
    logger.info(s"refreshToken: $tokenContainer")
    Akka.system.scheduler.scheduleOnce((getTokenContainer.expires_in - 300).seconds) {
      refreshToken()
    }
  }

  def getTokenContainer = tokenContainer match {
    case Some(tContainer) => tContainer
    case None => Try {
      authorize()
    } match {
      case Success(tContainer) => tokenContainer = Some(tContainer)
        tokenContainer.get
      case Failure(t) => throw new RuntimeException(t)
    }
  }

}

case class TokenContainer(
                           expires_in: Long,
                           token_type: String,
                           refresh_token: String,
                           access_token: String
                         )

case class AuthErrorResponse(
                              error: String,
                              error_description: String
                            )
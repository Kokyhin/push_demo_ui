package app

import app.global.ConfigLoader
import app.models.User
import play.api.{Application, GlobalSettings, Logger, Play}

object Starter extends GlobalSettings {
  val logger = Logger(this.getClass)

  override def onStart(app: Application): Unit = {
    val environment = ConfigLoader.load.getString("app.environment")
    logger.info(s"starting enviromnent $environment")
    User.findOneByEmail(User.default.email) match {
      case Some(user)=>
      case None => User.createAdminUserInDB()
    }
  }

  override def onStop(app: Application) {}
}
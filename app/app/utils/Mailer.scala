package app.utils

import javax.mail._
import javax.mail.internet._
import java.util._

import collection.JavaConversions._
import app.global.ConfigLoader.conf
import play.api.libs.Crypto


// Usage:
// Mailer.sendMail("Recipient name", "recipient email", "subject", "html text (without <html><body>")
// Don't forget to set up mail setting in application.conf
object Mailer extends App {
  lazy val mail = conf.getConfig("mail")
  lazy val username = mail.getString("user")
  lazy val password = Crypto.decryptAES(mail.getString("password"))
  lazy val from = mail.getString("from")
  lazy val displayName = mail.getString("display.name")

  def sendMail(to: String, email: String, subject: String, text: String): Unit = {
    // TSL is recommended
    val properties = PropertiesProvider.getTsl
    val session = Session.getDefaultInstance(properties)
    val message = new MimeMessage(session)
    message.setFrom(s"$displayName <$from>")
    message.addRecipients(Message.RecipientType.TO, s"$to <$email>")
    message.setSubject(subject)
    message.setContent(text, "text/html; charset=utf-8")
    message.setSentDate(new Date())
    Transport.send(message, username, password)
  }
}

object PropertiesProvider {
  lazy val tslProperties = readFromConfig("tsl")
  lazy val sslProperties = readFromConfig("ssl")
  lazy val unsecuredProperties = readFromConfig("unsecured")

  // Gmail: If don't work - check this page: https://www.google.com/settings/security/lesssecureapps
  def getTsl: Properties = tslProperties

  // Gmail: also can find https://accounts.google.com/DisplayUnlockCaptcha
  def getSsl: Properties = sslProperties

  // Gmail: don't send mail on all hosts (verified for mail.ru, mail.md)
  def getUnsecured: Properties = unsecuredProperties

  def readFromConfig(branch: String): Properties = {
    val mailConf = conf.getConfig(branch).entrySet()
    val properties = new Properties()
    mailConf.foreach(prop => properties.setProperty(prop.getKey, prop.getValue.unwrapped().toString))
    properties
  }
}

package app.global

import com.typesafe.config.ConfigFactory

/**
  * Created by user on 9/16/16.
  */
object ConfigLoader {
  val conf = load

  def load = ConfigFactory.load()

  def load(resourceName: String) = ConfigFactory.load(resourceName)
}

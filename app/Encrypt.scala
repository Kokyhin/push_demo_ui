
import java.security.SecureRandom

/**
  * Created by user on 10/3/16.
  */
object Encrypt extends App {
  val secret = args.head
  val value = args(1)
  val result = Crypto.encryptAES(value, secret)
  println(s"\n\nRESULT: $result\n\n")
}

object Decrypt extends App {
  val secret = args.head
  val value = args(1)
  val result = Crypto.decryptAES(value, secret)
  println(s"\n\nRESULT: $result\n\n")
}

object GenerateSecret extends App {
  val result = generateSecret
  println(s"\n\nRESULT: $result\n\n")

  def generateSecret = {
    val random = new SecureRandom()
    (1 to 64).map { _ =>
      (random.nextInt(75) + 48).toChar
    }.mkString.replaceAll("\\\\+", "/")
  }
}

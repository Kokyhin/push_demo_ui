#!/bin/sh

ROLE=$1

if [ -z "$ROLE" ]; then
    echo "need a role name"
    exit 1
fi

echo "creating directory structure"
mkdir -p $ROLE/{tasks,handlers,templates,files,vars,defaults,meta}
echo "creating main.yml files"
echo "---" | tee $ROLE/{tasks,handlers,vars,defaults,meta}/main.yml > /dev/null

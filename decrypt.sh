#!/usr/bin/env bash
# run as ./decrypt 'SECRET' 'ENCRYPTED_PASSWORD'
sbt "run-main Decrypt $1 $2"
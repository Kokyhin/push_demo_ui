import play.sbt.routes.RoutesKeys

name := """wildcard"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  ws,
  "commons-io" % "commons-io" % "2.4",
  "org.scala-lang" % "scala-library" % "2.11.7",
  "org.scala-lang" % "scala-reflect" % "2.11.7",
  "org.scala-lang" % "scala-compiler" % "2.11.7",
  "org.scala-lang.modules" % "scala-parser-combinators_2.11" % "1.0.4",
  "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4",
  "xalan" % "serializer" % "2.7.2",
  "org.apache.httpcomponents" % "httpclient" % "4.3.4",
  "org.apache.httpcomponents" % "httpcore" % "4.3.2",
  "junit" % "junit" % "4.12"
)

libraryDependencies += "javax.mail" % "mail" % "1.5.0-b01"

//Salat dependencies
libraryDependencies += "net.cloudinsights" %% "play-plugins-salat" % "1.5.9"

RoutesKeys.routesImport += "se.radley.plugin.salat.Binders._"

//Selenium
libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.52.0"

// For ObjectId to Json serialization
TwirlKeys.templateImports += "org.bson.types.ObjectId"

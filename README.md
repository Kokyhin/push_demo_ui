# Wildcard Demo App


## Prerequisites:
* Java 8+
* Actual version of [sbt](http://www.scala-sbt.org/) 
* Actual version of [MongoDB](https://www.mongodb.com/) 

###To start project locally:

1. Go to project folder
2. Type in console ```sbt start``` to run application
3. Open in browser [http://localhost:9000](http://localhost:9000)

###To deploy app run from project directory:

`ansible-playbook deploy-ms3.yml --vault-password-file ../../vaultpass --extra-vars "build='0.1' config=ms3-dev repo='https://bitbucket.org/mountainstatesoftware/push_demo_ui.git' branch='develop'"`

###Encryption:
In project home dir:

* To generate new **secret** run `./generate_secret.sh`

* To encrypt **password** run `./encrypt.sh 'secret' 'password'`

* If you want to decrypt encrypted password run `./decrypt.sh 'secret' 'encrypted_password'`

####Using of secret key:
After generating new secret and password encryption, you have two options of using this secret:

* Update **crypto_secret** variable in ansible secured config file by running `ansible-vault edit roles/scala/vars/password.yml --vault-password-file=../../vaultpass`

* Also you can add **crypto_secret** variable to ansible extra-vars: `ansible-playbook deploy-ms3.yml --vault-password-file ../../vaultpass --extra-vars "build='0.1' config=ms3-dev repo='https://bitbucket.org/mountainstatesoftware/push_demo_ui.git' branch='develop' crypto_secret='mycrypto_secret'"`

###To watch logs of deployed app:

1. ssh to host where is deployed app
2. `sudo su`
3. `docker ps` to see which docker containers in now run
4. find `{containerId}` of scala image
4. `docker logs -f {containerId}`

==================================================




###Default user login  credentials: 
* email: `astamov@wildcardpayments.com`
* password: `admin`

*To change them edit [conf/application.conf](conf/application.conf) file*